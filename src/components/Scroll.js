import React from 'react';
import './Scroll.css';

const Scroll = (props) => {
    return (
        <div id='scroller'>
            {props.children}
        </div>
    )

}

export default Scroll;