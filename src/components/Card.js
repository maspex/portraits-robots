import React from 'react';
import './Card.css';

const Card = ({ name, email, id }) => { //receiving props, and destructuring them inside the brackets directly, so that i can invoke them directly, could also do props.name etc...
    return (
        <div id= "card" className='tc dib pa3 ma2 grow bw2 shadow-5'>
            <img src={`https://robohash.org/${id}?200x200`} alt='portrait robot' />
            <div>
                <h2>{name.first} {name.last}</h2>
            </div>
        </div>
    );
}

export default Card;