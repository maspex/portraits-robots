import React, { Component } from 'react';
import { BrowserRouter as Router, Routes } from "react-router-dom";
import Cardlist from '../components/Cardlist';
import Searchbox from '../components/Searchbox';
import Scroll from '../components/Scroll';
import { people } from '../components/data/people';
import './App.css';
import logo from './logo.png';

class App extends Component {
    constructor() {
        super() //weird thing that calls the constructor of components
        this.state = {
            people: [],
            searchfield: ''
            // App now owns state that includes people, it's then allowed to change it
        }
    }

    componentDidMount() {
        this.setState({ people : people })
    }
    //built in, doesnt need to be called

    onSearchChange = (event) => {
        this.setState({ searchfield: event.target.value })
    }

    render() {
        const { people, searchfield } = this.state;
        const filteredPeople = people.filter(people =>{
            var fullName = people.name.first + ' ' + people.name.last;
            return fullName.toLowerCase().includes(searchfield.toLowerCase());
        }) //toLowerCase is a method that makes everything lowercase for coparison of search results
        if (people.length === 0) {
            return <h1 className='tc'>Looking for deviants...</h1>
        } else {
            return (
                <Router>
                <div className='tc'>
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className='f-headline lh-solid'>The Electric Eye</h1>
                    <Searchbox searchChange={this.onSearchChange}/>
                    <Scroll>
                    <Cardlist people={filteredPeople} />
                    </Scroll>
                </div>
                </Router>
            );
        }
        //because it's an object I have to say "this." so that it says "this" (which is the app) ".onSearchChange" equals searchChange
        // Scroll wraps around the cardlist comp
    }
}

export default App;